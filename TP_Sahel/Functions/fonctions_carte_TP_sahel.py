import geopandas as gpd

world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
cities = gpd.read_file(gpd.datasets.get_path('naturalearth_cities'))

#fabrique le gropandas des villes a partir du dataframe des villes
def map_data(data_metadata_sta,varname='',unit='mm/an'):
    data_metadata_sta = data_metadata_sta.copy()
    gdata_metadata_sta = gpd.GeoDataFrame(data_metadata_sta, 
                                      geometry = gpd.points_from_xy(data_metadata_sta.lon,
                                                                    data_metadata_sta.lat))
    
    ax = world.plot(color='white',edgecolor='gray',facecolor='lightgray')
    ax.set_facecolor('lightgray')
    if varname=='':
        gdata_metadata_sta.plot(ax=ax,color='k').set(ylim=(2, 25), xlim=(-20,15))
        for x, y, label in zip(gdata_metadata_sta.geometry.x, 
                               gdata_metadata_sta.geometry.y, 
                               gdata_metadata_sta.index):
            ax.annotate(label, xy=(x+1, y-0.5))
    else:
        gdata_metadata_sta.plot(ax=ax,color='skyblue',markersize=varname).set(ylim=(2, 25), xlim=(-20,15))
        for x, y, label,rain in zip(gdata_metadata_sta.geometry.x, 
                                    gdata_metadata_sta.geometry.y, 
                                    gdata_metadata_sta.index,
                                    gdata_metadata_sta[varname]):
            ax.annotate(label, xy=(x+2, y+0.5))
            txt = '('+str(int(rain))+' '+unit+')'
            ax.annotate(txt,xy=(x+2,y-0.5),fontsize=7)
    return ax
